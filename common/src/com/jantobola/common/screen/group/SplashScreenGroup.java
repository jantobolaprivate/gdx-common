package com.jantobola.common.screen.group;

import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;

/**
 * SplashScreenGroup
 *
 * @author Jan Tobola, 2015
 */
public class SplashScreenGroup extends AbstractScreenGroup {

    public SplashScreenGroup(ScreenManager screenManager) {
        super(screenManager);
    }

    @Override
    public ScreenType getScreenType() {
        return ScreenType.SPLASH;
    }

}
