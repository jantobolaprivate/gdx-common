package com.jantobola.common.screen.group;

import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;

/**
 * GameScreenGroup
 *
 * @author Jan Tobola, 2015
 */
public class GameScreenGroup extends AbstractScreenGroup {

    public GameScreenGroup(ScreenManager screenManager) {
        super(screenManager);
    }

    @Override
    public ScreenType getScreenType() {
        return ScreenType.GAME;
    }

}
