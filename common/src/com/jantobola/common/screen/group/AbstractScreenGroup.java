package com.jantobola.common.screen.group;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;
import com.jantobola.common.screen.loadable.LoadableScreen;

import java.util.HashMap;
import java.util.Map;

/**
 * AbstractScreenGroup is an abstract class holding all screens for a given screen group.
 * It is possible to have multiple screen groups. See {@link ScreenType}
 *
 * @author Jan Tobola, 2015
 */
public abstract class AbstractScreenGroup implements Screen {

	private String currentScreenId = "";
	protected LoadableScreen currentScreen;
	protected Map<String, LoadableScreen> screens = new HashMap<>();
	protected ScreenManager screenManager;

	public AbstractScreenGroup(ScreenManager screenManager) {
		this.screenManager = screenManager;
	}

	public AbstractScreenGroup setScreen(String id) {

		LoadableScreen screen = screens.get(id);

		if (screen != null) {
			this.currentScreen = screen;
			this.currentScreenId = id;
		}

		if (currentScreen != null)
			return this;

		throw new GdxRuntimeException("No screen with id: \"" + id + "\" exists in \"" + getClass().getSimpleName() + "\" with \"" + getScreenType()
				+ "\" screen type.");
	}

	public abstract ScreenType getScreenType();

	public void addScreen(String id, LoadableScreen screen) {
		screen.thisScreen = new LoadableScreen.ScreenInfo(id, getScreenType());
		screens.put(id, screen);
	}

	public void addScreen(String id, LoadableScreen screen, LoadableScreen.ScreenInfo nextScreen) {
		screen.nextScreen = nextScreen;
		addScreen(id, screen);
	}

	public void setNext(String id, LoadableScreen.ScreenInfo next) {
		screens.get(id).nextScreen = next;
	}

	public AbstractScreenGroup setNext(LoadableScreen.ScreenInfo next) {
		currentScreen.nextScreen = next;
		return this;
	}

	public LoadableScreen get() {
		return this.currentScreen;
	}

	public LoadableScreen get(String id) {
		return this.screens.get(id);
	}

	@Override
	public void show() {
		currentScreen.showDelegate();
	}

	@Override
	public void render(float delta) {
		currentScreen.renderDelegate(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void resize(int width, int height) {
		currentScreen.resizeDelegate(width, height);
	}

	@Override
	public void pause() {
		currentScreen.pauseDelegate();
	}

	@Override
	public void resume() {
		currentScreen.resumeDelegate();
	}

	@Override
	public void hide() {
		currentScreen.hideDelegate();
	}

	@Override
	public void dispose() {
		for (LoadableScreen loadableScreen : screens.values()) {
			loadableScreen.dispose();
		}
	}

}
