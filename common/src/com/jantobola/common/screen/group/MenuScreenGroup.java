package com.jantobola.common.screen.group;

import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;

/**
 * MenuScreenGroup
 *
 * Screen representing main menu and its sub parts like options (sounds options, graphics options, etc.).
 * Switching between menu parts (which are actually separate screens) is done by switching between multiple
 * Stages. Each stage has its own scene graph and knows how to render its screen.
 *
 * @author Jan Tobola, 2015
 */
public class MenuScreenGroup extends AbstractScreenGroup {

    public MenuScreenGroup(ScreenManager screenManager) {
        super(screenManager);
    }

    @Override
    public ScreenType getScreenType() {
        return ScreenType.MENU;
    }

}
