package com.jantobola.common.screen;

/**
 * ScreenType
 *
 * @author Jan Tobola, 2015
 */
public enum ScreenType {

        MENU,
        GAME,
        LOADING,
        SPLASH

}
