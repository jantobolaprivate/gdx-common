package com.jantobola.common.screen.loadable;

import com.jantobola.common.asset.AssetRequests;

/**
 * Loadable
 *
 * @author Jan Tobola, 2015
 */
public interface Loadable {

    AssetRequests getAssetRequests();

    boolean requiresAssets();

}
