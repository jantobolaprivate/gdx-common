package com.jantobola.common.screen.loadable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jantobola.common.screen.ScreenManager;

/**
 * Screen2D for 2D graphics, like 2D games, menu, loaded screen etc.
 *
 * @author Jan Tobola, 2015
 */
public abstract class Screen2D extends LoadableScreen {

    protected Stage stage = new Stage();

    public Screen2D(ScreenManager screenManager) {
        super(screenManager);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().setScreenSize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
