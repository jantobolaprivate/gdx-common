package com.jantobola.common.screen.loadable;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.jantobola.common.asset.AssetRequests;
import com.jantobola.common.asset.ReleasableAssetDescriptor;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;

/**
 * Every screen extending LoadableScreen can obtain all resources from AssetManager.
 *
 * @author Jan Tobola, 2015
 */
public abstract class LoadableScreen implements Loadable, Disposable {

    private AssetRequests assetRequests = new AssetRequests();
    protected ScreenManager screenManager;

    public ScreenInfo thisScreen;
    public ScreenInfo nextScreen;

    private boolean requiresAssets = false;

    /*
        Constructors
    */

    public LoadableScreen(ScreenManager screenManager) {
        this.screenManager = screenManager;
        requestAssets();
        requiresAssets = assetRequests.getRequests().size > 0;
    }

    /*
        Abstract methods
    */

    protected abstract void requestAssets();
    protected abstract void show();
    protected abstract void render(float delta);
    protected abstract void resize (int width, int height);
    protected abstract void pause ();
    protected abstract void resume ();
    protected abstract void hide ();

    /*
        Interface implementation
    */

    @Override
    public AssetRequests getAssetRequests() {
        return this.assetRequests;
    }

    /*
        Delegates
    */

    public <T> void requestAsset(String id, String file, Class<T> type) {
        assetRequests.<T>requestAsset(id, file, type, true);
    }

    public <T> void requestAsset(String id, FileHandle file, Class<T> type) {
        assetRequests.<T>requestAsset(id, file, type, true);
    }

    public <T> void requestAsset(String id, String file, Class<T> type, AssetLoaderParameters<T> params) {
        assetRequests.<T>requestAsset(id, file, type, params, true);
    }

    public <T> void requestAsset(String id, FileHandle file, Class<T> type, AssetLoaderParameters<T> params) {
        assetRequests.<T>requestAsset(id, file, type, params, true);
    }

    public <T> void requestAsset(String file, Class<T> type) {
        assetRequests.<T>requestAsset(file, file, type, true);
    }

    public <T> void requestAsset(FileHandle file, Class<T> type) {
        assetRequests.<T>requestAsset(file.path(), file, type, true);
    }

    public <T> void requestAsset(String file, Class<T> type, AssetLoaderParameters<T> params) {
        assetRequests.<T>requestAsset(file, file, type, params, true);
    }

    public <T> void requestAsset(FileHandle file, Class<T> type, AssetLoaderParameters<T> params) {
        assetRequests.<T>requestAsset(file.path(), file, type, params, true);
    }

    public <T> void requestAsset(String id, String file, Class<T> type, boolean releasable) {
        assetRequests.<T>requestAsset(id, file, type, releasable);
    }

    public <T> void requestAsset(String id, FileHandle file, Class<T> type, boolean releasable) {
        assetRequests.<T>requestAsset(id, file, type, releasable);
    }

    public <T> void requestAsset(String id, String file, Class<T> type, AssetLoaderParameters<T> params, boolean releasable) {
        assetRequests.<T>requestAsset(id, file, type, params, releasable);
    }

    public <T> void requestAsset(String id, FileHandle file, Class<T> type, AssetLoaderParameters<T> params, boolean releasable) {
        assetRequests.<T>requestAsset(id, file, type, params, releasable);
    }

    public <T> void requestAsset(String file, Class<T> type, boolean releasable) {
        assetRequests.<T>requestAsset(file, file, type, releasable);
    }

    public <T> void requestAsset(FileHandle file, Class<T> type, boolean releasable) {
        assetRequests.<T>requestAsset(file.path(), file, type, releasable);
    }

    public <T> void requestAsset(String file, Class<T> type, AssetLoaderParameters<T> params, boolean releasable) {
        assetRequests.<T>requestAsset(file, file, type, params, releasable);
    }

    public <T> void requestAsset(FileHandle file, Class<T> type, AssetLoaderParameters<T> params, boolean releasable) {
        assetRequests.<T>requestAsset(file.path(), file, type, params, releasable);
    }

    public void goToNextScreen() {
        screenManager.setScreen(nextScreen.type, nextScreen.id);
    }

    public void nextScreenLoaded() {
        screenManager.getScreen(nextScreen).setRequiresAssets(false);
    }

    /*
        CLassic methods
    */
    public void showDelegate() {
        requiresAssets = false;
        show();
    }

    public void renderDelegate(float delta) {
        render(delta);
    }

    public void resizeDelegate(int width, int height) {
        resize(width, height);
    }

    public void pauseDelegate() {
        pause();
    }

    public void resumeDelegate() {
        resume();
    }

    public void hideDelegate() {
        int refReleased = screenManager.releaseAssets(this);
        requiresAssets = refReleased > 0;
        hide();
    }

    protected <T> T getAsset(String id) {
        return screenManager.<T>fetchAsset((ReleasableAssetDescriptor<T>) assetRequests.getRequests().get(id));
    }

    /*
        Inner class
    */

    public static class ScreenInfo {
        public String id;
        public ScreenType type;

        public ScreenInfo(String id, ScreenType type) {
            this.id = id;
            this.type = type;
        }
    }

    /*
        Getter & Setters
    */

    @Override
    public boolean requiresAssets() {
        return this.requiresAssets;
    }

    public void setRequiresAssets(boolean requiresAssets) {
        this.requiresAssets = requiresAssets;
    }

}
