package com.jantobola.common.screen.loadable;

import com.jantobola.common.screen.ScreenManager;

/**
 * Screen3D aka game level.
 *
 * @author Jan Tobola, 2015
 */
public abstract class Screen3D extends LoadableScreen {

    public Screen3D(ScreenManager screenManager) {
        super(screenManager);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
