package com.jantobola.common.lua;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.jse.JsePlatform;

/**
 * LuaScript
 *
 * Example (call from Lua): <br>
 *
 * <code>
 *      require 'ClassName' <br>
 *      ClassName.functionToCall(arg)
 * </code>
 *
 * @author Jan Tobola, 2015
 */
public abstract class LuaScript<T> extends TwoArgFunction {

	private LuaValue library;
	protected T instance;

	public LuaScript() {

	}

	public LuaScript(final T instance) {
		this.instance = instance;
	}

	/**
	 * Called when 'require' is called from Lua script. Creates hash table
	 * with child class name and puts all functions in it.
	 *
     * @param modname modname
     * @param env environment
     * @return LuaValue
     */
	@Override
	public LuaValue call(final LuaValue modname, final LuaValue env) {

		library = LuaValue.tableOf();

		try {
			registerFunctions();
		} catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
			e.printStackTrace();
		}

		env.set(getClass().getSimpleName(), library);
		return library;
	}

	/**
	 * Registers Lua functions based on inner class names.
	 *
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 */
	private void registerFunctions() throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

		for (Class<?> innerClass : getClass().getDeclaredClasses()) {

			Class<?> enclosingClass = getClass();
			Object enclosingInstance = enclosingClass.newInstance();

			Constructor<?> constructor = innerClass.getDeclaredConstructor(enclosingClass);
			Object innerInstance = constructor.newInstance(enclosingInstance);

			if (innerInstance instanceof LuaFunction) {
				library.set(innerClass.getSimpleName(), (LuaFunction) innerInstance);
			}
		}
	}

	/**
	 * Util method that runs script from file.
	 *
     * @param scriptPath scriptPath
     * @return LuaValue
     */
	public static LuaValue runFromFile(String scriptPath) {

		Globals globals = JsePlatform.standardGlobals();
		LuaValue chunk = globals.loadfile(scriptPath);

		return chunk.call(LuaValue.valueOf(scriptPath));
	}

	/**
	 * Util method that runs a given script.
	 *
     * @param script script
     * @return LuaValue
     */
	public static LuaValue run(String script) {

		Globals globals = JsePlatform.standardGlobals();
		LuaValue chunk = globals.load(script);

		return chunk.call(LuaValue.valueOf(script));
	}

}
