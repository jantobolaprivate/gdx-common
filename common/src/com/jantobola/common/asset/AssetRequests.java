package com.jantobola.common.asset;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * AssetRequests is a structure holding all informations about resources that need to be loaded by
 * AssetManager for a given screen part.
 *
 * @author Jan Tobola, 2015
 */
public class AssetRequests {

    private ObjectMap<String, ReleasableAssetDescriptor> requests = new ObjectMap<>();

    public <T> void requestAsset(String id, String file, Class<T> type, boolean releasable) {
        requests.put(id, new ReleasableAssetDescriptor<T>(file, type, releasable));
    }

    public <T> void requestAsset(String id, FileHandle file, Class<T> type, boolean releasable) {
        requests.put(id, new ReleasableAssetDescriptor<T>(file, type, releasable));
    }

    public <T> void requestAsset(String id, String file, Class<T> type, AssetLoaderParameters<T> params, boolean releasable) {
        requests.put(id, new ReleasableAssetDescriptor<T>(file, type, params, releasable));
    }

    public <T> void requestAsset(String id, FileHandle file, Class<T> type, AssetLoaderParameters<T> params, boolean releasable) {
        requests.put(id, new ReleasableAssetDescriptor<T>(file, type, params, releasable));
    }

    public void clearRequests() {
        this.requests.clear();
    }

    public ObjectMap<String, ReleasableAssetDescriptor> getRequests() {
        return this.requests;
    }

}
