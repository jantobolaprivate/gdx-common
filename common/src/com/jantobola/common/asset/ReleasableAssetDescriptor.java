package com.jantobola.common.asset;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.files.FileHandle;

/**
 * ReleasableAssetDescriptor
 *
 * @author Jan Tobola, 2015
 */
public class ReleasableAssetDescriptor<T> extends AssetDescriptor<T> {

    private boolean releasable = true;

    public ReleasableAssetDescriptor(String fileName, Class<T> assetType, boolean releasable) {
        super(fileName, assetType);
        this.releasable = releasable;
    }

    public ReleasableAssetDescriptor(FileHandle file, Class<T> assetType, boolean releasable) {
        super(file, assetType);
        this.releasable = releasable;
    }

    public ReleasableAssetDescriptor(String fileName, Class<T> assetType, AssetLoaderParameters<T> params, boolean releasable) {
        super(fileName, assetType, params);
        this.releasable = releasable;
    }

    public ReleasableAssetDescriptor(FileHandle file, Class<T> assetType, AssetLoaderParameters<T> params, boolean releasable) {
        super(file, assetType, params);
        this.releasable = releasable;
    }

    public boolean isReleasable() {
        return releasable;
    }

    public void setReleasable(boolean releasable) {
        this.releasable = releasable;
    }

}
