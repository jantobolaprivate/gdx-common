package com.jantobola.common.entity.component.mapper;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Mapper
 *
 * @author Jan Tobola, 2015
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Mapper {

}
