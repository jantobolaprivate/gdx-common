package com.jantobola.common.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.jantobola.common.entity.EntityEngine;
import com.jantobola.common.entity.component.mapper.Mapper;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * EntitySystem
 *
 * @author Jan Tobola, 2015
 */
public abstract class EntitySystem extends com.badlogic.ashley.core.EntitySystem {

	protected ImmutableArray<Entity> entities;

	public EntitySystem() {
		super();
		createMappers();
	}

	public EntitySystem(int priority) {
		super(priority);
		createMappers();
	}

	@Override
	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(filter());
		addedCallback((EntityEngine) engine);
	}

	@Override
	public void removedFromEngine(Engine engine) {
		removedCallback((EntityEngine) engine);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
	}

    private void createMappers() {
        for (Field field : getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Mapper.class)) {
                Type t = field.getGenericType();
                try {
                    ComponentMapper mapper = ComponentMapper.getFor((Class) ((ParameterizedType) t).getActualTypeArguments()[0]);
                    field.setAccessible(true);
                    field.set(this, mapper);
                } catch (IllegalAccessException e) {
                    throw new GdxRuntimeException(e);
                }
            }
        }
    }

	protected abstract Family filter();

	protected abstract void addedCallback(EntityEngine engine);

	protected abstract void removedCallback(EntityEngine engine);

}
